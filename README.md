# Projecte Final: Cau de Jocs

Projecte final del grau 'Desenvolupament d'Aplicacions Web' a l'Escola del Treball, Barcelona.
Curs 2020 - 2021.

![logo](./Documentació/plantillaHTML/Photographer-master/img/logo.png)

## Contingut

Es tracta d'una aplicació web de mini-jocs. A n'aquesta n'hi trobem dos:

- Qui és qui?: és multijugador i conté, a més del joc, un xat mitjançant sockets amb el qual es poden fer les preguntes a l'altre jugador.

- Cercamines: es pot triar entre tres dificultats (fàcil, mitjà i difícil), les quals varien el número de bombes de la partida. Un cop finalitzada aquesta, es registra el temps que ha durat i, si es tracta de la millor marca, es guarda al perfil de l'usuari. De tots els temps que hi ha a la base de dades de tots els usuaris, es crea un rànquing general a la web.

A més dels jocs anteriors, també hi ha un apartat d'usuari, en el qual es poden modificar les dades d'aquest (nick, correu, contrasenya i imatge de perfil), veure les estadíístiques dels jocs (millor temps de cada dificultat al 'Cercamines' i el número de partides i victòries al 'Qui és qui?').

Per poder accedir a la web, cal registrar-se, ja que, en cas contrari, tots els continguts estaran restringits.  
