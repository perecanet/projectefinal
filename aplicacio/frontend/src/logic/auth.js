// Fitxer amb el codi pel registre i login d'usuaris

import UsuariDataService from "../services/usuariDataService";

export default {
  registre(nick, email, contrasenya, rol, imatge, self) {

    // Es fa el registre al back-end i un cop llest es redirecciona a la pàgina de login
    const user = { nick, email, contrasenya, rol, imatge };
    UsuariDataService.createUsuari(user).then(response => {
        self.flashMessage.info({
          title: 'Usuari registrat!',
          message: "Ja pots iniciar sessió"
        });
        self.$router.push("/")
        console.log("Registrat: " + response.data)
    }).catch(err => {
        console.log("Error: " + err);
        document.getElementById("errorNick").style.display = "inline";
        return err;
    })
  },
  login(nick, contrasenya, self) {

    // Es neteja el local storage i la sessió, es fa el login cridant a un controlador del back-end, s'emmagatzema el token a la sessió i local
    // storage (asíncronament) i després es redirecciona a la pàgina principal
    console.log(contrasenya)
    self.$session.destroy();
    localStorage.clear();
    var user = { nick: nick, contrasenya: contrasenya };
    UsuariDataService.login(user).then((response) => {
        self.$session.start()
        self.$session.set("id", response.data.id);
        self.$session.set("token", response.data.token)
        console.log(self.$session.getAll())

        const asyncLocalStorage = {
          setItem: function (key, value) {
            return Promise.resolve().then(function () {
                localStorage.setItem(key, value);
            });
        },
        getItem: function (key) {
            return Promise.resolve().then(function () {
                return localStorage.getItem(key);
            });
        }
        };

        asyncLocalStorage.setItem("token", response.data.token).then(function () {
          asyncLocalStorage.getItem('token').then(function (value) {
            console.log('Token a LOCAL STORAGE'+value);
            console.log("before push")
            self.$router.push('/jocs');
          }) 
        });
        console.log('Guardant token a LOCAL STORAGE');
    }).catch(err => {
      console.log("Error: " + err);
      document.getElementById("errorNick").style.display = "inline";
      return err;
  })
  }
};
