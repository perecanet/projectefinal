// Fitxer principal del front-end, on definim totes les llibreries i dependències que utilitzarem i inicialitzem l'objecte Vue

import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import axios from './http-common'
import BootstrapVue from 'bootstrap-vue';
import AxiosPlugin from 'vue-axios-cors';
import VueSession from 'vue-session';
import FlashMessage from '@smartweb/vue-flash-message';

Vue.use(router)
Vue.use(BootstrapVue)
Vue.use(axios)
Vue.use(AxiosPlugin)
Vue.use(VueSession)
Vue.use(FlashMessage);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
