// Fitxer de definició de rutes i comprovació de tokens als requests

import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

let router = new Router({

  //Definim totes les rutes
  mode: "history",
  routes: [
    {
      path: "/",
      name: "login",
      component: () => import("../components/Login/Login")
    },
    {
      path: "/registre",
      name: "registre",
      component: () => import("../components/Registre/Registre")
    },
    {
      path: "/usuari",
      name: "usuari",
      component: () => import("../components/Usuari/Usuari"),
      meta: { requiresAuth: true }
    },
    {
      path: "/assoliments",
      name: "assoliments",
      component: () => import("../components/Assoliments/Assoliments"),
      meta: { requiresAuth: true }
    },
    {
      path: "/cercamines",
      name: "cercamines",
      component: () => import("../components/Cercamines/Cercamines"),
      meta: { requiresAuth: true }
    },
    {
      path: "/configuracio",
      name: "configuracio",
      component: () => import("../components/Configuracio/Configuracio"),
      meta: { requiresAuth: true }
    },
    {
      path: "/jocs",
      name: "jocs",
      component: () => import("../components/Jocs/Jocs"),
      meta: { requiresAuth: true }
    },
    {
      path: "/quiesqui",
      name: "quiesqui",
      component: () => import("../components/Quiesqui/Quiesqui"),
      meta: { requiresAuth: true }
    },
    // En cas que la ruta no sigui cap de les anteriors, redirecciona a la pàgina 404
    { 
      path: '/:catchAll(.*)', 
      component: () => import("../components/404/404"),
      name: 'NotFound'
    }
  ]
});

// Comprova la presencia de token al local storage abans de permetre l'accés a les pàgines
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    
    if (!localStorage.getItem('token')) {
      next({
        path: '/'
        
      })
      
    }
    else if(Vue.prototype.$session.get("id") === undefined){
      next({
        path: '/'
      })
    }
    else {
      next()
    }
  } else {
    next()
  }
})

export default router

