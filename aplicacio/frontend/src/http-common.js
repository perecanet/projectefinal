// Fitxer on establim la connexió amb el backend, iniciant-la ja amb dos headers

import axios from "axios";

const ajax = axios.create({
   baseURL: "http://10.200.242.224:4001",
   headers: {
    "Content-type": "application/json",
    "Access-Control-Allow-Origin": "Content-Type"
   }
})

ajax.CancelToken = axios.CancelToken
ajax.isCancel = axios.isCancel

// Funció asíncrona per obtenir el token
const asyncLocalStorage = {
  setItem: function (key, value) {
    return Promise.resolve().then(function () {
        localStorage.setItem(key, value);
    });
  },
  getItem: function (key) {
      return Promise.resolve().then(function () {
          return localStorage.getItem(key);
      });
  }
};

/*
 * L'interceptor comprova que cada request contingui un token i l'afegeix al header 'Authorization'
 */

ajax.interceptors.request.use(
  async (config) =>  {
    var token=await asyncLocalStorage.getItem('token');

    console.log('INTERCEPTOR Token' + token);

    if (token) {
      console.log("tocket in Auth");
      ajax.defaults.headers.common["Authorization"] = token
    }

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default ajax
