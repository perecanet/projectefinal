// Totes les peticions a les rutes d'usuari de l'API

import http from "../http-common";

class UsuariDataService {
  //USUARIS
  getUsuari(id) {
    return http.get(`/usuari/${id}`);
  }
  
  login(data) {
    return http.post(`/usuari`, data);
  }

  createUsuari(data) {
    return http.post(`/usuari/registre`, data);
  }

  deleteUsuari(id) {
    return http.delete(`/usuari/eliminar/${id}`);
  }

  updateUsuari(id, data) {
    return http.put(`/usuari/modificar/${id}`, data);
  }

  savepartida(id, data) {
    return http.put(`/usuari/partidafinalitzada/${id}`, data);
  }
}

export default new UsuariDataService();
