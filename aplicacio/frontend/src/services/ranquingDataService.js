// Totes les peticions a les rutes de rànquing de l'API

import http from "../http-common";

class RanquingDataService {
  //RANQUING
  getRanquing(dificultat) {
    return http.get(`/ranquing/llista/${dificultat}`);
  }

  postRanquing(data) {
    return http.post(`/ranquing/llista`, data);
  }

  userDificultats(nick) {
    return http.get(`/ranquing/llistaDificultats/${nick}`);
  }

  userMillorTempsDif(nick, dificultat) {
    return http.get(`/ranquing/llistaUsuariDificultat/${nick}?dificultat=${dificultat}`);
  }

  eliminarRanquingsUsuari(data) {
    return http.delete(`/ranquing/eliminarUsuari/${data}`);
  }
}

export default new RanquingDataService();
