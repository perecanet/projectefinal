import http from "../http-common";

class JocDataService {
  //JOCS
  getJocs() {
    return http.get(`/joc/llista`);
  }

  getJoc(data) {
      return http.get(`/joc/?nom=${data.nom}`);
  }
}

export default new JocDataService();
