import http from "../http-common";

class SalaDataService {
  //SALA
  getSales() {
    return http.get(`/sala/llista`);
  }

  getSalesJoc(joc) {
    return http.get(`/sala/llistajoc?joc=${joc}`);
  }

  getSala(nom) {
    return http.get(`/sala/?nom=${nom}`);
  }

  nouUsuari(data) {
    return http.put(`/sala/noujugador`, data);
  }

  llevarUsuari(data) {
    return http.put(`/sala/llevarjugador`, data);
  }
}

export default new SalaDataService();
