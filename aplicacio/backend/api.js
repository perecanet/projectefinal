require("dotenv").config();
var express = require("express");
var api = express();
var server = require('http').createServer(api);
var mongoose = require("mongoose")
var path = require("path");
var body_parser = require('body-parser');
var io = require("socket.io");
var cors = require("cors");
const events = require('./events');

/**
 * Iniciar el port del servidor
 */
server.listen(process.env.API_PORT, (err,res)=>{
    if(err) console.log("Error del servidor"+ err);
    else console.log("API connectat");
})

/**
 * Connexió de la BD mitjançant mongoose
 */
mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
    if (err) console.log(`ERROR: connecting to Database. ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

var apiRoutes = require("./app/routes/api");
var usuariRoutes = require("./app/routes/usuari");
var salaRoutes = require("./app/routes/sala");
var ranquingRoutes = require("./app/routes/ranquing");
var jocRoutes = require("./app/routes/joc");

/**
 * Definició de les dependencies que s'utilitzarà
 */
api.use(body_parser.json());
api.use(body_parser.urlencoded({extended:true}));
api.use(cors());

/**
 * Inicialitzar les rutes de la API
 */
api.use("/", apiRoutes);
api.use("/usuari", usuariRoutes);
api.use("/sala", salaRoutes);
api.use("/ranquing", ranquingRoutes);
api.use("/joc", jocRoutes);

/**
 * Informar a el socket el servidor i carregar els events del socket
 */
io = io(server);
new events(io).eventsConfig();
