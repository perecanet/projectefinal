var mongoose = require("mongoose"),
    Sala = require("../models/sala");
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
    
/**
 * Carregar totes les sales
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.load = async (req,res,next) => {
   var result = await Sala.find({});
   res.jsonp(result);
};

/**
 * Carregar totes les sales del joc pasat
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.loadby = async (req,res,next) => {
    var nomJoc = {nomJoc:req.query.joc}
    console.log(nomJoc);
    var result = await Sala.find(nomJoc);
    res.jsonp(result);
 };

 /**
  * carregar una sala per el seu nom
  * @param {ç} req 
  * @param {*} res 
  * @param {*} next 
  */
exports.findOne = async (req, res, next) => {  
    var input = {nom: req.query.nom}
    var result = await Sala.find(input);
    res.jsonp(result);
};

/**
 * Guardar la id d'un usuari en la llista de una sala pasada per el seu nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.nouusuari = async (req, res, next) => { 
    console.log("entra"); 
    var input = {nom: req.body.sala}
    var result = await Sala.find(input);
    var usuari = result[0];
    usuari.idUsuaris.push(req.body.usuari);
    console.log(usuari);
    var sala = {nom: usuari.nom};
    var resultmod = await Sala.findOneAndUpdate(sala, usuari);
    res.jsonp(resultmod);
};

/**
 * Eliminar la id d'un usuari en la llista de una sala pasada per el seu nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.llevarusuari = async (req, res, next) => { 
    console.log("surt");
    var input = {nom: req.body.sala}
    var result = await Sala.find(input);
    var usuari = result[0];
    const index = usuari.idUsuaris.indexOf(usuari.id);
    if(usuari.idUsuaris[0] == usuari.id){
        usuari.idUsuaris.shift();
    }
    else{
        usuari.idUsuaris.pop();
    }
    console.log(usuari);
    var sala = {nom: usuari.nom};
    var resultmod = await Sala.findOneAndUpdate(sala, usuari);
    res.jsonp(resultmod);
};

/**
 * Guardar una nova sala
 */
exports.save = (urlencodedParser,async (req,res,next) =>{
    var newSala = new Sala(req.body);
    console.log(req.body);
    newSala.save((err,result)=>{
        if(err){
            console.log("Error: " + err);
            result = "Error insertant a la base de dades."
        }
        else {
            console.log("Insertat a la BD");
            result = "Ok"
        }
        res.jsonp(result);
    })
});

/**
 * Eliminar una sala per el seu Nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.delete = async(req,res,next) => {
    var input = {nom:req.params.id}
    console.log(input);
    var result = await Sala.deleteOne(input);
    res.jsonp(result);
};

/**
 * Modificar unsa sala per el seu Nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.modify = async(req,res,next) =>{
    var input = {nom:req.params.id}
    console.log(input);
    var result = await Sala.updateOne(input, req.body);
    res.jsonp(result);
};
