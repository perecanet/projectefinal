var mongoose = require("mongoose"),
    Usuari = require("../models/usuari");
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var saltRounds = 10;
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jwt = require('jsonwebtoken');

/**
 * Mètode per carregar tots els usuaris de la base de dades
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.load = async(req,res,next) => {
    var result = await Usuari.find({});
    res.jsonp(result);
};

/**
 * Mètode per verificar login
 * 
 */
exports.login = (urlencodedParser,async(req,res,next)=> {  
    var input = {nick:req.body.nick}
    console.log(input);
    await Usuari.find(input,(err,result) => {
        console.log(result)
        if(result.length > 0){
            if(!bcrypt.compareSync(req.body.contrasenya, result[0].contrasenya)) {
                return res.status(400).jsonp({
                    error: 1,
                    msg: "Nick i/o contrasenya incorrecta"  
                  })
            }
            console.log("Usuari trobat");
            let token = jwt.sign({
                data: result[0]
              }, 'secret', { expiresIn: 60 * 60 * 24 * 30}) // Expira en 30 dies     
              console.log(token)
            var resultat = {
                token: token,
                id:  result[0]._id
            }    
            return res.json(resultat)
        }
        else {
            console.log("Error: " + err);
            return res.status(400).jsonp({
              error: 1,
              msg: "Nick i/o contrasenya incorrectes"  
            })
        }
    })
});

/**
 * Mètode per actualitzar estadístiques del 'qui és qui?'
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.savegame = async(req,res,next)=>{
    var input = {_id:req.params.id}
    var resultatPartida = req.body.resultat;
    console.log(input);
    await Usuari.find(input,async (err,result) => {
        console.log(result)
        if(result.length > 0){
            var usuari = result[0];
            if(usuari.numPartides === null){
                usuari.numPartides = 0;
            }
            if(usuari.numVictories === null){
                usuari.numVictories = 0;
            }
            usuari.numPartides = usuari.numPartides+1;
            if(resultatPartida){
                usuari.numVictories = usuari.numVictories+1;
            }
            await Usuari.findOneAndUpdate(input, usuari).exec((err, result) => {
                if(err){
                    console.log("Error: " + err);
                    return res.status(400).jsonp({
                      error: 1,
                      msg: "Error modificant l'usuari."  
                    })
                }
                else {
                    console.log("Usuari modificat.");
                    res.jsonp("Ok");
                }
            });
        }
        else {
            console.log("Error: " + err);
            return res.status(400).jsonp({
              error: 1,
              msg: "Usuari no trobat"  
            })
        }
    })
}

/**
 * Mètode per buscar un usuari mitjançant l'id
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.findUsuari = async(req,res,next)=> {  
    var input = {_id:req.params.id}
    console.log(input);
    await Usuari.find(input,(err,result) => {
        console.log(result)
        if(result.length > 0){
            console.log("TROBAT")
            return res.json(result)
        }
        else {
            console.log("Error: " + err);
            return res.status(400).jsonp({
              error: 1,
              msg: "Usuari no trobat"  
            })
        }
    })
};

/**
 * Mètode per insertar un nou usuari
 * 
 */
exports.save = (urlencodedParser,async (req,res,next) =>{
    var body = {
        nick: req.body.nick,
        rol: req.body.rol,
        email: req.body.email,
        imatge: req.body.imatge
    }
    body.contrasenya = bcrypt.hashSync(req.body.contrasenya, saltRounds);

    var newUsuari = new Usuari(body);
    
    console.log(req.body);
    
    newUsuari.save((err,result)=>{
        if(err){
            console.log("Error: " + err);
            return res.status(400).jsonp({
              error: 1,
              msg: "El nick ja existeix"  
            })
        }
        else {
            console.log("Insertat a la BD");
            res.jsonp("Ok");
        }
    })
});

/**
 * Esborrar un usuari mitjançant id
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.delete = async(req,res,next) => {
    var input = {_id:req.params.id}
    console.log(input);
    await Usuari.deleteOne(input, (err, result) => {
        if(err){
            console.log("Error: " + err);
            return res.status(400).jsonp({
              error: 1,
              msg: "Error eliminant l'usuari."  
            })
        }
        else {
            console.log("Usuari eliminat.");
            res.jsonp("Ok");
        }
    });
};

// Mètode per modificar un usuari amb contrasenya encriptada
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.modify = async(req,res,next) =>{
    var input = {_id: req.params.id};
    var body = req.body;
    
    console.log(body);
    console.log(input);

    if(body.contrasenya) {
        body.contrasenya = bcrypt.hashSync(req.body.contrasenya, saltRounds);
    }

    await Usuari.findOneAndUpdate(input, body).exec((err, result) => {
        if(err){
            console.log("Error: " + err);
            return res.status(400).jsonp({
              error: 1,
              msg: "Error modificant l'usuari."  
            })
        }
        else {
            console.log("Usuari modificat.");
            res.jsonp("Ok");
        }
    });
};
