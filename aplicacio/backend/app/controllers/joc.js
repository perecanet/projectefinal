var mongooose = require("mongoose"),
    Joc = require("../models/joc");
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

/**
 * Carregar els joc de la BD
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.load = async(req,res,next) => {
    var result = await Joc.find({});
    res.jsonp(result);
};

/**
 * Carregar un joc de la BD per nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.findOne = async(req,res,next)=> {  
    var input = {nom:req.query.nom}
    var result = await Joc.find(input);
    res.jsonp(result);
};

/**
 * Guardar un joc a la BD
 */
exports.save = (urlencodedParser,async (req,res,next) =>{
    var newJoc = new Joc(req.body);
    console.log(req.body);
    newJoc.save((err,result)=>{
        if(err){
            console.log("Error: " + err);
            result = "Error insertant a la base de dades."
        }
        else {
            console.log("Insertat a la BD");
            result = "Ok"
        }
        res.jsonp(result);
    })
});

/**
 * Eliminar un joc de la BD per nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.delete = async(req,res,next) => {
    var input = {nom:req.params.id}
    console.log(input);
    var result = await Joc.deleteOne(input);
    res.jsonp(result);
};

/**
 * Modificar un joc de la BD per nom
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.modify = async(req,res,next) =>{
    var input = {nom:req.params.id}
    console.log(input);
    var result = await Joc.updateOne(input, req.body);
    res.jsonp(result);
};
