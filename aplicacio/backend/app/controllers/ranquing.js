var mongoose = require("mongoose"),
    Ranquing = require("../models/ranquing");
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

/**
 * Mètode per carregar 10 millors temps independentment de la dificultat
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.load = (req,res,next) => {
    Ranquing.find({}).sort({'millorTemps':1}).limit(10).exec((err,result) => {
     if(err){
         console.log("Error :"+err);
         return res.status(400).jsonp({
             error: 1,
             msg: "No s'ha pogut conectar a la base de dades."
         })
     }
     else {
         console.log("Ranquing trobat");
         return res.jsonp(result)
     }
    });
};

/**
 * Mètode per carregar 10 millors temps segons la dificultat
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.findDificultat = (req,res,next)=> {
    Ranquing.find({ dificultat : req.params.dificultat}).sort({'temps':1}).limit(10).exec((err,result) => {
        if(err){
            console.log("Error :"+err);
            return res.status(400).jsonp({
                error: 1,
                msg: "No s'ha pogut conectar a la base de dades."
            })
        }
        else {
            console.log("Ranquing trobat");
            console.log(result);
            return res.jsonp(result)
        }
    });
}


/**
 *Mètode per carregar les dificultats
 *  
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.findDistinctDificultats = (req,res,next)=> {
    Ranquing.find({nick: req.params.nick}).distinct('dificultat').exec((err,result) => {
        if(err){
            console.log("Error :"+err);
            return res.status(400).jsonp({
                error: 1,
                msg: "No s'ha pogut conectar a la base de dades."
            })
        }
        else {

            console.log("Ranquing trobat");
            console.log(result);
            return res.jsonp(result)
        }
    });
}

/**
 * Mètode per carregar millor temps d'un usuari segons dificultat
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.findUsuariDificultat = (req,res,next)=> {
    Ranquing.find({nick: req.params.nick, dificultat: req.query.dificultat}).sort({'temps':1}).limit(1).exec((err,result) => {
        if(err){
            console.log("Error :"+err);
            return res.status(400).jsonp({
                error: 1,
                msg: "No s'ha pogut conectar a la base de dades."
            })
        }
        else {

            console.log("MILLOR TEMPS USUARI");
            console.log(result);
            return res.jsonp(result)
        }
    });
}

/**
 * Carregar tots els rànquings d'un usuari
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.findOne = async(req,res,next)=> {  
    var input = {nick : req.query.nick}
    var result = await Ranquing.find(input);
    res.jsonp(result);
};

/**
 * Mètode per guardar un nou rànquing
 */
exports.save = (urlencodedParser,async (req,res,next) =>{
    var newRanking = new Ranquing(req.body);
    console.log(req.body);
    newRanking.save((err,result)=>{
        if(err){
            console.log("Error: " + err);
            result = "Error insertant a la base de dades."
        }
        else {
            console.log("RANQUING Insertat");
            result = "Ok"
        }
        res.jsonp(result);
    })
});

/**
 * Mètode per esborrar un rànquing d'un usuari
 */
exports.delete = async(req,res,next) => {
    var input = {nick : req.params.id}
    console.log(input);
    var result = await Ranquing.deleteOne(input);
    res.jsonp(result);
};

/**
 * Mètode per esborrar tots els rànquings d'un usuari
 */
exports.deleteUserRanking = async(req,res,next) => {
    var input = {nick: req.params.id}
    console.log(input);
    var result = await Ranquing.deleteMany(input);
    res.jsonp(result);
};

/**
 * Mètode per modificar un rànquing
 */
exports.modify = async(req,res,next) =>{
    var input = {nick : req.params.id}
    console.log(input);
    var result = await Ranquing.updateOne(input, req.body);
    res.jsonp(result);
};
