//Routes de 'Sala' del la API

var path = require("path");
var express = require("express");
var router = express.Router();
var {verificarAuth} = require('../auth/autenticar.js');
var {verificarAdmin} = require('../auth/autenticarAdmin.js');

var salaCntr = require(__dirname+"/../controllers/sala");

router.route("/llista").get(verificarAdmin,salaCntr.load);
router.route("/llistajoc").get(verificarAuth,salaCntr.loadby);
router.route("/").get(verificarAuth,salaCntr.findOne);
router.route("/noujugador").put(verificarAuth,salaCntr.nouusuari);
router.route("/llevarjugador").put(verificarAuth,salaCntr.llevarusuari);
router.route("/nova").post(verificarAdmin,salaCntr.save);
router.route("/eliminar/:id").delete(verificarAdmin,salaCntr.delete);
router.route("/modificar/:id").put(verificarAdmin,salaCntr.modify);

module.exports = router;
