// Totes les rutes de usuari de l'API

var path = require("path");
var express = require("express");
var router = express.Router();
var {verificarAuth} = require('../auth/autenticar.js');
var {verificarAdmin} = require('../auth/autenticarAdmin.js');

var usuariCntr = require(__dirname+"/../controllers/usuari");

router.route("/registre").post(usuariCntr.save);
router.route("/").post(usuariCntr.login);
router.route("/partidafinalitzada/:id").put(verificarAuth,usuariCntr.savegame);
router.route("/:id").get(verificarAuth,usuariCntr.findUsuari);
router.route("/eliminar/:id").delete(verificarAuth,usuariCntr.delete);
router.route("/modificar/:id").put(verificarAuth,usuariCntr.modify);
router.route("/llista").get(verificarAdmin,usuariCntr.load);

module.exports = router;
