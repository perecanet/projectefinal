// Totes les rutes de rànquing de l'API

var path = require("path");
var express = require("express");
var router = express.Router();
var {verificarAuth} = require('../auth/autenticar.js');
var {verificarAdmin} = require('../auth/autenticarAdmin.js');

var ranquingCntr = require(__dirname+"/../controllers/ranquing");

router.route("/llista/:dificultat").get(verificarAuth,ranquingCntr.findDificultat);
router.route("/llistaDificultats/:nick").get(verificarAuth,ranquingCntr.findDistinctDificultats);
router.route("/llistaUsuariDificultat/:nick").get(verificarAuth,ranquingCntr.findUsuariDificultat);
router.route("/llista").post(verificarAuth,ranquingCntr.save);

router.route("/llista").get(verificarAuth,ranquingCntr.load);
router.route("/").get(verificarAuth,ranquingCntr.findOne);
router.route("/eliminarUsuari/:id").delete(verificarAuth,ranquingCntr.deleteUserRanking)
router.route("/eliminar/:id").delete(verificarAdmin,ranquingCntr.delete);
router.route("/modificar/:id").put(verificarAdmin,ranquingCntr.modify);


module.exports = router;
