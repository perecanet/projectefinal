//Routes de 'Joc' del la API

var path = require("path");
var express = require("express");
var router = express.Router();
var {verificarAuth} = require('../auth/autenticar.js');
var {verificarAdmin} = require('../auth/autenticarAdmin.js');

var jocCntr = require(__dirname+"/../controllers/joc");

router.route("/llista").get(verificarAuth,jocCntr.load);
router.route("/").get(verificarAuth,jocCntr.findOne);
router.route("/nou").post(verificarAdmin,jocCntr.save);
router.route("/eliminar/:id").delete(verificarAdmin,jocCntr.delete);
router.route("/modificar/:id").put(verificarAdmin,jocCntr.modify);

module.exports = router;
