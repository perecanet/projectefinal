// Fitxer per autenticar usuaris 'Admin'

var jwt = require('jsonwebtoken');

let verificarAdmin = (req, res, next) => {

  // Llegir headers
  let token = req.headers.authorization;

  // Verificació del token
  jwt.verify(token, 'secret', (err, decoded) => {
    if(err) {
      console.log("ERROR DE TOKEN")
      return res.status(401).json({
        msg: 'Error de token',
        err
      })
    }
    // Cream propietat amb la info de l'usuari
    res = decoded.data; //data ve al generar el token al findOne del controlador d'usuari
    
    // Comprovem que l'usuari tingui rol 'admin'
    if(res.rol != "admin") {
      console.log("ERROR DE ROL")
      return res.status(401).json({
        msg: 'Error de ROL',
        err
      })
    }
    next();
  });
}
module.exports = {verificarAdmin};
