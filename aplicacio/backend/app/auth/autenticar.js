// Fitxer per autenticar usuaris 'Jugadors'

var jwt = require('jsonwebtoken');

let verificarAuth = (req, res, next) => {
  // Llegir headers
  let token = req.headers.authorization;
  
  // Comprovació que el token sigui vàlid
  jwt.verify(token, 'secret', (err, decoded) => {
    if(err) {
      console.log("ERROR DE TOKEN")
      return res.status(401).json({
        msg: 'Error de token',
        err
      })
    }
    // Cream propietat amb la info de l'usuari
    res = decoded.data; //data ve al generar el token al findOne del controlador d'usuari

    next();
  });
}
module.exports = {verificarAuth};
