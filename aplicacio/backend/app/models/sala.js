/**
 * Model de Sala en Mongoose
 */

 var mongoose = require('mongoose'),
 Schema = mongoose.Schema;

var salaSchema = new Schema({
 idUsuaris: {type: [Schema.Types.ObjectId]},
 nom: {type: String, unique: true},
 capacitat: {type: Number},
 nomJoc: {type: String}
});

module.exports = mongoose.model("Sala", salaSchema);
