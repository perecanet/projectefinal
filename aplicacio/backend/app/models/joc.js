/**
 * Model de Joc en Mongoose
 */

 var mongoose = require('mongoose'),
 Schema = mongoose.Schema;

var jocSchema = new Schema({
 nom: {type: String, unique: true},
 imatge: {type: String},
 tipus: {type: String}
});

module.exports = mongoose.model("Joc", jocSchema);
