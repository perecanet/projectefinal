// Model de l'esquema usuari de mongoose

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var usuariSchema = new Schema({
    nick: {type: String, unique: true},
    rol: {type: String},
    contrasenya: {type: String},
    email: {type: String},
    imatge: {type: String},
    numPartides: {type: Number, default: 0},
    numVictories: {type: Number, default: 0}
});

usuariSchema.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.contrasenya;
    return obj;
}

module.exports = mongoose.model("Usuari", usuariSchema);
