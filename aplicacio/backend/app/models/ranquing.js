// Model de l'esquema ranquing de mongoose

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ranquingSchema = new Schema({
    nick: {type: String},
    temps: {type: String},
    dificultat: {type: String}
});

module.exports = mongoose.model("Ranquing", ranquingSchema);
