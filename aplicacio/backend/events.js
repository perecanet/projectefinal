class Events {
  constructor(io){
    this.io = io;
  }

  socketEvents(io) {
    /**
     * Es conecta un socket client
     */
    io.on('connection', (socket) => {
      console.log("usuario Conectado");
      /**
       * S'inicia un socket client a la sala pasada i s'inicia el seu camp d'image mab el seu id
       */
      socket.on('connected', async (user) => {
        await socket.join(user.sala);
        if(!io.nsps['/'].adapter.rooms[user.sala].img){
          io.nsps['/'].adapter.rooms[user.sala].img = [];
          io.nsps['/'].adapter.rooms[user.sala].img.push({id:user.id, img:""});
        }
        else{
          //si el id ja existeix la llista de images
          var existeix = false;
          io.nsps['/'].adapter.rooms[user.sala].img.forEach(usuari => {
            console.log("usuari1");
            console.log(usuari);
            console.log("usuari2");
            console.log(user.id);
            if(usuari.id == user.id){
              //existeix
              existeix = true;
            }
          });
          var users = io.nsps['/'].adapter.rooms[user.sala].length;
          console.log(users);
          //si ja hi ha el limit(ha entrat posant la url)
          if(users > 2){
            socket.emit("entrar",false);
            socket.disconnect();
          }
          //si ja existeix a la llista de images
          if(!existeix){
            io.nsps['/'].adapter.rooms[user.sala].img.push({id:user.id, img:""});
            socket.emit("entrar",true);
          }
          //Hi ha espai lliure i no hi es a la llista de images
          else{
            socket.emit("entrar",false);
            socket.disconnect();
          }
        }
      });
      /**
       * El socket es desconecta
       */
      socket.on('disconnect', () => {
        console.log("Desconectat");
      });
      /**
       * El socket envia um missatge rebut a tots els altres sockets de la sala
       */
      socket.on('message', (message) => {
        console.log(message);
        socket.to(message.sala).broadcast.emit('message', message.message);
      });
      /**
       * El socket indica que es pot comensar en joc
       * Si hi ha 2 jugadors s'envia una peticio start a els sockets i la seva posició
       */
      socket.on('start', async (sala) => {
        var usuariosSala = await io.nsps['/'].adapter.rooms[sala.sala].length;
        var user1 = io.nsps['/'].adapter.rooms[sala.sala].img[0].id;
        if(usuariosSala == 2){
          socket.in(sala.sala).emit('start', {start:true});
          socket.emit('start', {start:true});
          if(sala.id == user1){
            socket.emit('primer', true);
          }
          else{
            socket.emit('primer',false);
          }
        }
      });
      /**
       * Envia la image seleccionada(la del usuari) i la guarda a la llista de images a la mateixa posició que el seu id
       */
      socket.on('image', (image) => {
        console.log("image");
        console.log(io.nsps['/'].adapter.rooms[image.sala].img[0].id);
        console.log(io.nsps['/'].adapter.rooms[image.sala].img[1].id);
        console.log(image.img);
        if(io.nsps['/'].adapter.rooms[image.sala].img[0].id == image.id){
          io.nsps['/'].adapter.rooms[image.sala].img[0].img = image.img;
        }
        else if(io.nsps['/'].adapter.rooms[image.sala].img[1].id == image.id){
          io.nsps['/'].adapter.rooms[image.sala].img[1].img = image.img;
        }
        console.log(io.nsps['/'].adapter.rooms[image.sala]);
        //envia que ja ha seleccionat la image
        socket.to(image.sala).broadcast.emit('imagecontrincant',true);
      })
      /**
       * Resultat final del socket(image seleccio final)
       * Comprova la posicio del usuari i compara la image amb la del altre.
       * Si es iguala, envia una peticio 'guanyar' true al usuari i una false al contrincant
       * Si no hes iguala, al reves
       */
      socket.on('final', (image) => {
        console.log(image);
        if(io.nsps['/'].adapter.rooms[image.sala].length == 2){
        if(io.nsps['/'].adapter.rooms[image.sala].img[0].id != image.id){
          console.log("0");
          console.log(io.nsps['/'].adapter.rooms[image.sala].img[0].img);
          console.log(image.img);
          if(io.nsps['/'].adapter.rooms[image.sala].img[0].img == image.img){
            console.log("0.0");
            socket.emit('guanyar',true);
            socket.to(image.sala).broadcast.emit('guanyar',false);
          }
          else{
            console.log("0.1");
            socket.emit('guanyar',false);
            socket.to(image.sala).broadcast.emit('guanyar',true);
          }
        }
        else if(io.nsps['/'].adapter.rooms[image.sala].img[1].id != image.id){
          console.log("1");
          if(io.nsps['/'].adapter.rooms[image.sala].img[1].img == image.img){
            console.log("1.0");
            socket.emit('guanyar',true);
            socket.to(image.sala).broadcast.emit('guanyar',false);
          }
          else{
            console.log("1.1");
            socket.emit('guanyar',false);
            socket.to(image.sala).broadcast.emit('guanyar',true);
          }
        }
        else{
          socket.disconnect();
        }
      }
      })
      /**
       * Informa que l'usuari ha sortit de la sala
       */
      socket.on('leave', async (sala) => {
        console.log("leave");
        await socket.leave(sala);
        socket.disconnect();
      });
    });
  }

  /**
   *  Initialize socket events
   */

  eventsConfig() {
    this.socketEvents(this.io);
  }
}

module.exports = Events;
